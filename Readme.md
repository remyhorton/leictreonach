Leichreonach: Electronics catalogue tool
=======================================
_Leichreonach_ is a complete rewrite of [wxCatalogue][v2.x] and is a
tool that was developed to allow me to keep track of electronic components
I had ordered in for various projects.
It is written for Python 3 using the PYGObject interface to GTK 3, with
[SQLite][sqli] being used for data storage.

The project website is at [Leictreonach.eu][leic].


## Current status
I actively use this program to maintain my own component catalogue, so development
is adding and fixing things as I need them.


## Release versions

### Version 3.1
This version adds a list view in addition to the tree-based view, as well as code to check the consistency/sanity of the database. Several bugs have also been fixed over the last two and a half years so it was time to bump the version number.

### Version 3.0
Initial release using GTK and PyGObject, and renamed to _Leictreonach_ due to
change of GUI toolkit. Since _Leictreonach_ is a continuation of _wxCatalogue_ 
I decided to maintain the existing version numbering.

### Version 2.x
A rewrite of _wxCatalogue_ in C++ that used wxWidgets v3.1.3 directly, the main
changes being a switch to a tree list control and the use of an SQLite backend.

### Version 1.x
Original utility that used [wxPython][wxpy] and Python 2 and later rewritten to
use Python 3. No longer maintained.


## Future plans
There are some presentation issues that need polishing, such as icons and some of
the popup dialogs, but functionally the only planned feature that is missing is
export of data to JSON. For now main goal is locating and fixing any bugs.
There is an experimental C-based version but this is not intended to supplant the
main Python-based version.


## Licence
_Leictreonach_ is licenced under [version 3 of the GPL][gpl3].


## Contact
Send emails to ``remy.horton`` (at) ``gmail.com``

[v2.x]: https://bitbucket.org/remyhorton/wxcatalogue/src/v2.x/
[wxwi]: https://www.wxwidgets.org/
[sqli]: https://www.sqlite.org/
[wxpy]: https://wxpython.org/
[gpl3]: https://www.gnu.org/licenses/gpl.html
[leic]: https://leictreonach.eu/
