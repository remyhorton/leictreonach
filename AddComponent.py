## AddComponent.py - New component adding dialog
#
# (c) Remy Horton, 2021
# SPDX-License-Identifier: GPL-3.0-only


import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject

from NameDialog import NameDialog

class AddComponentHandler(object):
    def __init__(self, hdlMain):
        self.hdlMain = hdlMain
        self.db = hdlMain.db
        self.dialog = hdlMain.builderAddComp.get_object('dialogAddComponent')
        self.dialog.set_transient_for(hdlMain.window)
        self.builder = hdlMain.builderAddComp
        self.txtName = self.builder.get_object('txtTitle')
        self.txtCode = self.builder.get_object('txtCode')
        self.txtDesc = self.builder.get_object('txtDesc')
        self.txtNote = self.builder.get_object('txtNotes')
        self.ctrlManu = self.builder.get_object('ctrlManu')
        self.txtManu = self.builder.get_object('txtManu')

        ## Vendor list ctrl
        self.modelVendors = Gtk.ListStore(
            GObject.TYPE_STRING, GObject.TYPE_STRING
            )
        self.listVendors = self.builder.get_object('treeVendors')
        self.listVendors.set_model(self.modelVendors)
        # Vendor column
        cell = Gtk.CellRendererText()
        cell.set_property('editable',True)
        cell.connect('edited',self.evtEditVendNameDone)
        col = Gtk.TreeViewColumn('Vendor',cell,text=0)
        col.set_resizable(True)
        col.set_property('min-width',100)
        self.listVendors.append_column(col)
        # Order code column
        cell = Gtk.CellRendererText()
        cell.set_property('editable',True)
        cell.connect('edited',self.evtEditVendCodeDone)
        self.listVendors.append_column(
            Gtk.TreeViewColumn('Order code',cell,text=1))

        # Attach Glade-specified signals
        self.builder.connect_signals(self)

        # This should really be specified in Glade XML..
        self.txtName.set_hexpand(True)

    def evtVendorAdd(self, button):
        listVendNames = [v for (_,v) in self.db.getVendNames()]
        dlgName = NameDialog(
            self.dialog,
            "Select vendor",
            "Select component vendor",
            listVendNames)
        strVendor = dlgName.getValue()
        if strVendor is not None:
            self.modelVendors.append([strVendor,''])

    def evtVendorDel(self, button):
        victim = self.listVendors.get_selection().get_selected()[1]
        if victim is not None:
            self.modelVendors.remove(victim)

    def evtEditVendNameDone(self, item, path, newText):
        treeiter = self.modelVendors.get_iter( path )
        self.modelVendors.set_value(treeiter, 0, newText)

    def evtEditVendCodeDone(self, item, path, newText):
        treeiter = self.modelVendors.get_iter( path )
        self.modelVendors.set_value(treeiter, 1, newText)

    def evtSave(self, button):
        if self.evtAnother(button):
            self.dialog.hide()

    def evtAnother(self, button):
#        model,iterCategory = self.treeCategories.get_selection().get_selected()
#        if iterCategory:
#            #print("Cat: {0} ({1})".format(
#            #    model.get_value(iterCategory,0),
#            #    model.get_value(iterCategory,1)))
#            idParent = model.get_value(iterCategory,1)
#            #print(">>",self.treeCategories.get_path())
#        else:
#            #self.showError("Must select a category")
#            idParent = None
#            #return False
        strName = self.txtName.get_text()
        strCode = self.txtCode.get_text()
        strDesc = self.txtDesc.get_text()
        strManu = self.txtManu.get_text()
        if len(strName) == 0:
            self.showError("Must specify a component name")
            return False
        if len(strManu) == 0:
            self.showError("Must specify a manufacturer name")
            return False
        if len(strCode) == 0:
            self.showError("Must specify an item code")
            return False
        strNote = None
        listVend = []
        for name,code in self.modelVendors:
            listVend.append( (name,code) )
        idNew = self.hdlMain.db.createPart(
            self.idParent, strName, strManu, strCode, strDesc, strNote, listVend, None, self.newPos)
        if self.newPos is not None:
            self.newPos += 1
        self.listNew.append(idNew)
        self.clear()
        return True

    def showError(self, strError):
        dialogMsg = Gtk.MessageDialog(
            parent=self.dialog,
            flags=Gtk.DialogFlags.MODAL,
            type=Gtk.MessageType.ERROR,
            buttons=Gtk.ButtonsType.OK,
            message_format=strError
            )
        dialogMsg.connect("response", lambda w,r : dialogMsg.destroy())
        dialogMsg.show()

    def show(self, itemPos = None):
        self.ctrlManu.remove_all()
        for _,manu in self.db.getNames('manufacturer').items():
            self.ctrlManu.append_text(manu)
        self.listNew = []
        listCats = self.hdlMain.db.getCategories()
        #pathOld = self.treeCategories.get_cursor()
        self.newPos = None
        self.idParent = None
        if itemPos:
            if not self.db.getItemIsCategory(itemPos):
                itemCat = self.hdlMain.modelMain.iter_parent(itemPos)
                if itemCat.user_data == 0:
                    itemCat = None
                self.newPos = self.db.getItemPosition(itemPos)
            else:
                itemCat = itemPos
        else:
            itemCat = None
        if itemCat is not None:
            self.idParent = itemCat.user_data
        #FIXME: Use itemDefaultCat
        #if pathOld[0] is not None:
        #    self.treeCategories.set_cursor(pathOld[0])
        #self.treeCategories.set_cursor()
        self.dialog.run()
        self.dialog.hide()
        return self.listNew

    def clear(self):
        self.txtName.set_text('')
        self.txtCode.set_text('')
        self.txtDesc.set_text('')
        self.txtManu.set_text('')
        self.modelVendors.clear()

