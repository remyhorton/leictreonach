## ManageNames.py - Location and Manufacturer name management
#
# (c) Remy Horton, 2021
# SPDX-License-Identifier: GPL-3.0-only


import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject

class ManageNamesHandler(object):
    def __init__(self, hdlMain):
        #self.main = hdlMain
        builder = hdlMain.builderManageNames
        self.dialogNames = builder.get_object('dialogNames')
        self.dialogMerge = builder.get_object('dialogMerge')
        self.dialogNames.set_transient_for(hdlMain.window)
        builder.connect_signals(self)

        self.imgTick = Gtk.IconTheme.get_default().load_icon(
                "gtk-apply",Gtk.IconSize.MENU,0)
        self.imgCross = Gtk.IconTheme.get_default().load_icon(
                "gtk-no",Gtk.IconSize.MENU,0)
        self.imgCross = None
#        self.imgCross = Gtk.IconTheme.get_default().load_icon(
#                "gtk-stop",Gtk.IconSize.MENU,0)

        column = Gtk.TreeViewColumn("Name")
        colIcon = Gtk.CellRendererPixbuf()
        colText = Gtk.CellRendererText()
        colText.set_property('editable',True)
        colText.connect('edited',self.evtEditName)
        column.pack_start(colIcon,False)
        column.pack_start(colText,True)
        column.add_attribute(colText, "text",  0)
        column.add_attribute(colIcon, "pixbuf", 2)
        column.set_cell_data_func(colIcon, self.getRowIcon)
        #colTree.set_property('min-width',300)
        #colTree.set_resizable(True)
        self.listNames = builder.get_object('treeNames')
        self.listNames.append_column(column)

        self.modelNames = Gtk.ListStore(
            GObject.TYPE_STRING, GObject.TYPE_INT,GObject.TYPE_BOOLEAN)
        self.listNames.set_model(self.modelNames)
        self.listNames.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)

        self.modelTarget = Gtk.ListStore(str,int)
        self.modelTarget.append( ["name1",1] )
        self.modelTarget.append( ["name2",2] )
        self.listTarget = builder.get_object('ctrlMergeList')
        self.listTarget.append_column(
            Gtk.TreeViewColumn("name",Gtk.CellRendererText(),text=0))
        self.listTarget.set_model(self.modelTarget)


    def getRowIcon(self, column, cell, model, iterItem, user_data):
        if model.get_value(iterItem,2):
            cell.set_property('pixbuf', self.imgTick)
        else:
            cell.set_property('pixbuf', self.imgCross)

    def evtNamesMerge(self, button):
        if self.listNames.get_selection().count_selected_rows() < 2:
            return
        modelNames,listPaths = self.listNames.get_selection().get_selected_rows()
        usedDest = False
        self.modelTarget.clear()
        for path in listPaths:
            iterRow = modelNames.get_iter(path)
            nameRow = modelNames.get_value(iterRow,0)
            idRow = modelNames.get_value(iterRow,1)
            if modelNames.get_value(iterRow,2):
                usedDest = True
            self.modelTarget.append( [nameRow,idRow] )
        action = self.dialogMerge.run()
        self.dialogMerge.hide()
        if action == Gtk.ResponseType.CANCEL:
            return
        modelDest,iterDest = self.listTarget.get_selection().get_selected()
        if iterDest is None:
            return
        idDest = modelDest.get_value(iterDest,1)
        nameDest = modelDest.get_value(iterDest,0)
        listVictims = []
        listIDs = []
        for path in listPaths:
            iterRow = modelNames.get_iter(path)
            idRow = modelNames.get_value(iterRow,1)
            if idRow == idDest:
                modelNames.set_value(iterRow,2,True)
                continue
            listIDs.append(idRow)
            listVictims.append(iterRow)
        for iterRow in listVictims:
            modelNames.remove(iterRow)
        self.listMerges.append( (idDest,listIDs) )

    def evtNamesDelete(self, button):
        modelNames,listPaths = self.listNames.get_selection().get_selected_rows()
        if listPaths == []:
            return
        for path in listPaths:
            iterRow = modelNames.get_iter(path)
            if modelNames.get_value(iterRow,2):
                name = modelNames.get_value(iterRow,0)
                self.showInUse(name)
                return
        listVictims = []
        for path in listPaths:
            idSQL = modelNames.get_value(iterRow,1)
            modelNames.remove(iterRow)
            listVictims.append(idSQL)
        self.listDeleted.extend(listVictims)

    def evtEditName(self, cellrend, path, newText):
        treeiter = self.modelNames.get_iter([0])
        while True:
            pathItem = self.modelNames.get_path(treeiter)
            # Don't allow duplicate names
            if path != str(pathItem):
                if newText == self.modelNames.get_value(treeiter,0):
                    self.showInUse(newText)
                    return
            treeiter = self.modelNames.iter_next(treeiter)
            if treeiter is None:
                break


        treeiter = self.modelNames.get_iter( path )
        self.modelNames.set_value(treeiter, 0, newText)
        self.dictRenames[ self.modelNames.get_value(treeiter,1) ] = newText

    def showInUse(self,name):
        dialogMsg = Gtk.MessageDialog(
            parent=self.dialogNames,
            flags=Gtk.DialogFlags.MODAL,
            type=Gtk.MessageType.ERROR,
            buttons=Gtk.ButtonsType.OK,
            message_format="Name \"{0}\" is in use".format(name)
            )
        dialogMsg.set_title("Error")
        dialogMsg.run()
        dialogMsg.destroy()

    def process(self, listNames, strType):
        self.dialogNames.set_title("Manage {0} names".format(strType))
        self.listDeleted = []
        self.listMerges = []
        self.dictRenames = dict()
        self.modelNames.clear()
        for idSQL,name,used in listNames:
            self.modelNames.append( [name,idSQL,used] )
        action = self.dialogNames.run()
        self.dialogNames.hide()

        if action == Gtk.ResponseType.CANCEL:
            return None
        return (self.dictRenames.items(),self.listMerges,self.listDeleted)
