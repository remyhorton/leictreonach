#!/usr/bin/env python3
## Leictreonach.py - Electronic component catalogue (main entrypoint)
#
# (c) Remy Horton, 2021
# SPDX-License-Identifier: GPL-3.0-only

import gi
#import signal
#import sqlite3
import sys
#import warnings

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, GdkPixbuf, Gdk

from Debugging import printFunc


class StockModel(GObject.GObject, Gtk.TreeModel, Gtk.TreeDragSource, Gtk.TreeDragDest):
    #FLAG_ISCAT = 1

    def __init__(self, hdlMain):
        self.hdlMain = hdlMain
        self.db = hdlMain.db
        GObject.GObject.__init__(self)

    def getItemFromId(self, idSQL):
        idParent = self.db.sqlGetItem(
            "SELECT idParent FROM node WHERE id IS ?",
            (idSQL,))
        item = Gtk.TreeIter()
        item.user_data = idSQL
        item.user_data2 = idParent
        item.user_data3 = 0
        return item

    def isNodeCategory(self, item):
        idItem = self.db.sqlGetItem(
            "SELECT idItem FROM node WHERE id IS ?",
            (item.user_data,))
        if idItem is None:
            return True
        return False


    def do_get_flags(self):
        return Gtk.TreeModelFlags.ITERS_PERSIST

    def do_get_n_columns(self):
        return 6

    def do_get_column_type(self, idx):
        if idx == 1:
            return GdkPixbuf.Pixbuf
        else:
            return str

    def do_get_iter(self, path):
        #printFunc(self,path)
        listIdx = path.get_indices()
        idSQL = None
        for idx in listIdx:
            idParent = idSQL
            idSQL = self.db.sqlGetItem(
                "SELECT id FROM node WHERE idParent IS ?"+
                " ORDER BY position LIMIT 1 OFFSET ?",
                (idSQL,idx))
            if idSQL is None:
                return (False,None)
        item = Gtk.TreeIter()
        item.user_data = idSQL
        item.user_data2 = idParent
        item.user_data3 = 666
        return (True,item)

    def do_iter_has_child(self, item):
        count = self.db.sqlGetItem(
            "SELECT COUNT(*) FROM node WHERE idParent=?",
            (item.user_data,)
            )
        if count is None or count == 0:
            return False
        return True

    def do_iter_n_children(self, item):
        #printFunc(self)
        if item is None:
            return 0
        count = self.db.sqlGetItem(
            "SELECT COUNT(*) FROM node WHERE idParent=?",(item.user_data,))
        return count

    def do_get_value(self, item, idxCol):
        #printFunc(self,(item.user_data,idxCol))
        if True:
            if idxCol == 0:
                return self.db.sqlGetRow(
                    "SELECT name FROM node WHERE id=?",
                    (item.user_data,))
            idItem = self.db.sqlGetRow(
                "SELECT idItem FROM node WHERE id=?",
                (item.user_data,))[0]
            if idxCol == 1:
                if idItem is None:
                    return self.hdlMain.imgCategory
                return self.hdlMain.imgComponent
            if idItem is None:
                return ''
            rowItem = self.db.sqlGetRow(
                "SELECT idManufact,code FROM component WHERE id=?",
                (idItem,))
            if idxCol == 2:
                return self.db.sqlGetRow(
                    "SELECT name FROM manufacturer WHERE id=?",
                    (rowItem[0],))[0]
            if idxCol == 3:
                return rowItem[1]
            if idxCol == 4:
                listCodes = self.db.sqlGetRows(
                    "SELECT idVendor,code FROM ordercode WHERE idItem=?",
                    (idItem,))
                listVendors = []
                for idVend,strCode in listCodes:
                    strVend = self.db.sqlGetRow(
                        "SELECT name FROM vendor WHERE id=?",(idVend,))[0]
                    listVendors.append( "{0}:{1}".format(strVend,strCode) )
                return ", ".join(listVendors)
            if idxCol == 5:
                listStock = self.db.sqlGetRows(
                    "SELECT idLocation,count FROM stock WHERE idItem=?",
                    (idItem,))
                listReserves = []
                for idLoc,strCount in listStock:
                    strLoc = self.db.sqlGetRow(
                        "SELECT name FROM location WHERE id=?",(idLoc,))[0]
                    listReserves.append( "{0}:{1}".format(strLoc,strCount) )
                return ", ".join(listReserves)
            return None

    def setValue(self, item, idxCol, newValue):
        if idxCol == 0:
            field = 'name'
        elif idxCol == 1:
            return self.hdlMain.imgCategory
        else:
            return None
        return self.db.sqlExec(
            "UPDATE node SET {0}=? WHERE id=?".format(field),
            (newValue,item.user_data))

    def do_iter_next(self, item):
        pos = self.db.sqlGetItem(
            "SELECT position FROM node WHERE id=?",(item.user_data,))
        #printFunc(self,"","Pos {0}".format(pos))
        if item.user_data2 == 0:
            idParent = None
        else:
            idParent = item.user_data2
        idNext = self.db.sqlGetItem(
            "SELECT id FROM node WHERE idParent is ? AND position>?"+
            "ORDER BY position LIMIT 1",
            (idParent,pos,))
        if idNext is None:
            return (False, None)
        # For reasons I have forgotten this function has to recycle the passed-in
        # Gtk.TreeIter rather than create a new one as do_iter_nth_child() does..
        item.user_data = idNext
        return (True,item)

    def do_get_path(self, item):
        query = self.db.sql.cursor()
        idItem = item.user_data
        idParent = item.user_data2
        listPos = []
        while True:
            if idParent == 0:
                rows = query.execute(
                    "SELECT id FROM node WHERE idParent IS NULL ORDER BY position"
                    ).fetchall()
            else:
                rows = query.execute(
                    "SELECT id FROM node WHERE idParent=? ORDER BY position",
                    (idParent,)
                    ).fetchall()
            listIDs = [x[0] for x in rows]
            try:
                pos = listIDs.index(idItem)
            except ValueError:
                #FIXME: Put some sort of assert here instead..
                printFunc(
                    self,
                    (item.user_data,item.user_data2,item.user_data3),
                    "{0} {1}".format(listIDs,idItem)
                    )
                sys.exit(1)
            listPos.insert(0,pos)
            if idParent == 0:
                break
            idItem = idParent
            idParent = self.db.sqlGetItem(
                "SELECT idParent FROM node where ID=?",(idParent,))
            if idParent is None:
                idParent = 0
        query.close()
        return Gtk.TreePath(listPos)

    def do_iter_children(self, item):
        #printFunc(self)
        idParent = item.user_data
        idSQL = self.db.sqlGetItem(
            "SELECT id FROM node WHERE idParent=? ORDER BY position LIMIT 1",
            (idParent,))
        if idSQL is None:
            return (False, None)
        newItem = Gtk.TreeIter()
        newItem.user_data = idSQL
        newItem.user_data2 = idParent
        newItem.user_data3 = 666
        return (True, newItem)

    def do_iter_nth_child(self, item, idx):
        if item is None:
            idItem = None
        else:
            idItem = item.user_data
        value = self.db.sqlGetItem(
            "SELECT id FROM node WHERE idParent IS ? ORDER BY position"+
            " LIMIT 1 OFFSET ?",
            (idItem,idx,)
            )
        if value is None:
            return (False, None)
        newItem = Gtk.TreeIter()
        newItem.user_data = value
        newItem.user_data2 = idItem
        newItem.user_data3 = 666
        return (True,item)

    def do_iter_parent(self, item):
        idSQL = self.db.sqlGetItem(
            "SELECT idParent FROM node WHERE id=? ORDER BY position LIMIT 1",
            (item.user_data2,))
        if idSQL is None:
            idSQL = 0
        if False:
            item.user_data = item.user_data2
            item.user_data2 = idSQL
            return (True, item)
        else:
            newItem = Gtk.TreeIter()
            newItem.user_data = item.user_data2
            newItem.user_data2 = idSQL
            newItem.user_data3 = 0
            return (True, newItem)


    def do_row_draggable(self, path):
        return True

    def evtDragDataGet(self, tree, context, selection, idTarget, time):
        model,item = tree.get_selection().get_selected()
        atom = Gdk.Atom.intern("CategoryNode",False)
        self.iterFrom = item
        payload = item.user_data.to_bytes(4,'big')
        #print("Payload>",payload)
        selection.set(atom, 8, payload)

    def do_drag_data_get(self, path, selection):
        #atom = Gdk.Atom.intern("m33p",False)
        #selection.set(atom, 8, b'\xde\xad\xbe\xef')
        return True

    def evtDropDataRecv(self, tree, context, x, y, selection, idTarget, time):
        #printFunc(self,(x,y));
        model = tree.get_model()
        payload = selection.get_data()
        #print("Payload>",payload)
        pktType = payload[0]
        idSrc = int.from_bytes(payload, byteorder='big')
        infoDest = tree.get_dest_row_at_pos(x,y)
        if infoDest is None:
            return
        posDirection = {
            Gtk.TreeViewDropPosition.BEFORE         : -1,
            Gtk.TreeViewDropPosition.INTO_OR_BEFORE :  0,
            Gtk.TreeViewDropPosition.INTO_OR_AFTER  :  0,
            Gtk.TreeViewDropPosition.AFTER          :  1,
            }[infoDest[1]]
        pathDest = infoDest[0]
        iterDest = model.get_iter(pathDest)
        nameDest = self.do_get_value(iterDest,0)
        query = self.db.sql.cursor()

        pathOld = self.do_get_path(self.iterFrom)
        if pathOld == pathDest and posDirection == 0:
            context.finish(False,False,time)
            return

        # Disallow dropping item onto descendent
        listIdxSrc = pathOld.get_indices()
        listIdxDst = pathDest.get_indices()
        lenIdxSrc = len(listIdxSrc)
        lenIdxDst = len(listIdxDst)
        if lenIdxDst > lenIdxSrc:
            if listIdxDst[:lenIdxSrc] == listIdxSrc:
                return

        # Disallow component nodes having children
        idItem = query.execute(
            "SELECT idItem FROM node WHERE id = ?",
            (iterDest.user_data,)).fetchone()[0]
        if idItem is not None and posDirection == 0:
            posDirection = -1

        self.iterFrom = None
        # Shuffle up nodes at source
        posSrc,idSrcParent = query.execute(
            "SELECT position,idParent FROM node WHERE id=?",
            (idSrc,)
            ).fetchone()
        #printFunc(self,"","Source id",idSrc,"pos",posSrc,"parent",idSrcParent)
        query.execute(
            "UPDATE node SET position=position-1"
            " WHERE idParent IS ? AND position>?",
            (idSrcParent,posSrc))
        # Move category to new position
        movedItem = Gtk.TreeIter()
        movedItem.user_data = idSrc
        movedItem.user_data3 = 12
        self.row_deleted(pathOld)
        if posDirection == -1:
            posDest,idDestParent = query.execute(
                "SELECT position,idParent FROM node WHERE id=?",
                (iterDest.user_data,)
                ).fetchone()
            #printFunc(self,"","Target id",iterDest.user_data,"pos",posDest,"parent",idDestParent)
            query.execute(
                "UPDATE node SET position=position+1"
                " WHERE idParent IS ? AND position>=?",
                (idDestParent,posDest))
            query.execute(
                "UPDATE node SET position=?,idParent=? WHERE id=?",
                (posDest,idDestParent,idSrc))
            movedItem.user_data2 = idDestParent
        elif posDirection == 1:
            #FIXME: Also go here if target not category
            posDest,idDestParent = query.execute(
                "SELECT position,idParent FROM node WHERE id=?",
                (iterDest.user_data,)
                ).fetchone()
            #printFunc(self,"","Target id",iterDest.user_data,"pos",posDest,"parent",idDestParent)
            query.execute(
                "UPDATE node SET position=position+1"
                " WHERE idParent IS ? AND position>?",
                (idDestParent,posDest))
            query.execute(
                "UPDATE node SET position=?,idParent=? WHERE id=?",
                (posDest+1,idDestParent,idSrc))
            movedItem.user_data2 = idDestParent
        else:
            query.execute(
                "UPDATE node SET position=position+1"
                " WHERE idParent=?",
                (iterDest.user_data,))
            query.execute(
                "UPDATE node SET position=0,idParent=? WHERE id=?",
                (iterDest.user_data,idSrc))
            movedItem.user_data2 = iterDest.user_data

        path = self.do_get_path(movedItem)
        self.row_inserted(path, movedItem)
        # Force-expand to (new) parent. Moved row itself should stay collapsed.
        listSteps = path.get_indices()
        if len(listSteps) > 1:
            subpath = Gtk.TreePath.new_from_indices(listSteps[:-1])
            self.hdlMain.treeMain.expand_to_path(subpath)
        else:
            self.hdlMain.treeMain.expand_to_path(path)
        self.hdlMain.treeMain.set_cursor(path,None,False)
        # Doing a row_inserted on a possibly non-existent child row makes
        # Gtk.TreeView display the expander '+' if there are sub-nodes.
        # I suspect passed-in Gtk.TreeIter() is not actually referenced.
        listSteps = path.get_indices()
        listSteps.append( 0 )
        subpath = Gtk.TreePath.new_from_indices(listSteps)
        self.row_inserted(subpath, Gtk.TreeIter())
        # Finish up.
        context.finish(True,True,time)
        self.hdlMain.isChanges = True

    def do_row_drop_possible(self, path, selection):
        #print("do_row_drop_possible({0})".format(path));
        return False

    def do_drag_data_delete(self, path):
        return True

    def do_drag_data_received(self, path, selection):
        return True


class StockModelList(GObject.GObject, Gtk.TreeModel, Gtk.TreeDragSource):
    def __init__(self, hdlMain):
        self.hdlMain = hdlMain
        self.db = hdlMain.db
        GObject.GObject.__init__(self)

    def getItemFromId(self, idSQL):
        item = Gtk.TreeIter()
        item.user_data = idSQL
        item.user_data2 = None
        item.user_data3 = 0
        return item

    def do_get_flags(self):
        return Gtk.TreeModelFlags.ITERS_PERSIST

    def do_get_n_columns(self):
        return 6

    def do_get_column_type(self, idx):
        if idx == 1:
            return GdkPixbuf.Pixbuf
        else:
            return str

    def do_get_iter(self, path):
        listIdx = path.get_indices()
        idSQL = self.db.sqlGetItem(
            "SELECT id FROM node WHERE idItem IS NOT NULL" +
            " ORDER BY id LIMIT 1 OFFSET ?",
            (listIdx[0],))
        if idSQL is None:
            return (False,None)
        item = Gtk.TreeIter()
        item.user_data = idSQL
        item.user_data2 = None
        item.user_data3 = 0
        return (True,item)

    def do_iter_has_child(self, item):
        return False

    def do_iter_n_children(self, item):
        #printFunc(self)
        if item is None:
            return self.db.sqlGetRow(
                "SELECT count(id) FROM node WHERE idItem IS NOT NULL")
        return 0

    def do_get_value(self, item, idxCol):
        #printFunc(self,(item,idxCol))
        if item is None:
            return None
        if True:
            if idxCol == 0:
                return self.db.sqlGetRow(
                    "SELECT name FROM node WHERE id=?",
                    (item.user_data,))
            idItem = self.db.sqlGetRow(
                "SELECT idItem FROM node WHERE id=?",
                (item.user_data,))
            if idItem is None:
                printFunc(self,None,"Tried to fetch invalid id")
                return None
            idItem = idItem[0]
            if idxCol == 1:
                if idItem is None:
                    return self.hdlMain.imgCategory
                return self.hdlMain.imgComponent
            if idItem is None:
                return ''
            rowItem = self.db.sqlGetRow(
                "SELECT idManufact,code FROM component WHERE id=?",
                (idItem,))
            if idxCol == 2:
                return self.db.sqlGetRow(
                    "SELECT name FROM manufacturer WHERE id=?",
                    (rowItem[0],))[0]
            if idxCol == 3:
                return rowItem[1]
            if idxCol == 4:
                listCodes = self.db.sqlGetRows(
                    "SELECT idVendor,code FROM ordercode WHERE idItem=?",
                    (idItem,))
                listVendors = []
                for idVend,strCode in listCodes:
                    strVend = self.db.sqlGetRow(
                        "SELECT name FROM vendor WHERE id=?",(idVend,))[0]
                    listVendors.append( "{0}:{1}".format(strVend,strCode) )
                return ", ".join(listVendors)
            if idxCol == 5:
                listStock = self.db.sqlGetRows(
                    "SELECT idLocation,count FROM stock WHERE idItem=?",
                    (idItem,))
                listReserves = []
                for idLoc,strCount in listStock:
                    strLoc = self.db.sqlGetRow(
                        "SELECT name FROM location WHERE id=?",(idLoc,))[0]
                    listReserves.append( "{0}:{1}".format(strLoc,strCount) )
                return ", ".join(listReserves)
            return None

    def setValue(self, item, idxCol, newValue):
        if idxCol != 0:
            return None
        field = 'name'
        return self.db.sqlExec(
            "UPDATE node SET {0}=? WHERE id=?".format(field),
            (newValue,item.user_data))

    def do_iter_next(self, item):
        listRows = self.db.sqlGetRows(
            "SELECT id FROM node WHERE idItem IS NOT NULL" +
            " ORDER BY id")
        listRows = [x[0] for x in listRows]
        idxRow = listRows.index(item.user_data) + 1
        if idxRow  == len(listRows):
            return (False, None)
        item.user_data = listRows[idxRow]
        return (True,item)

    def do_get_path(self, item):
        listRows = self.db.sqlGetRows(
            "SELECT id FROM node WHERE idItem IS NOT NULL" +
            " ORDER BY id")
        listRows = [x[0] for x in listRows]
        idxRow = listRows.index(item.user_data) + 1
        return Gtk.TreePath( [idxRow] )

    def do_iter_children(self, item):
        #printFunc(self)
        idParent = item.user_data
        idSQL = self.db.sqlGetItem(
            "SELECT id FROM node WHERE idParent=? ORDER BY position LIMIT 1",
            (idParent,))
        if idSQL is None:
            return (False, None)
        newItem = Gtk.TreeIter()
        newItem.user_data = idSQL
        newItem.user_data2 = idParent
        newItem.user_data3 = 666
        return (True, newItem)

    def do_iter_nth_child(self, item, idx):
        if item is None:
            idItem = None
        else:
            idItem = item.user_data
        value = self.db.sqlGetItem(
            "SELECT id FROM node WHERE idParent IS ? ORDER BY position"+
            " LIMIT 1 OFFSET ?",
            (idItem,idx,)
            )
        if value is None:
            return (False, None)
        newItem = Gtk.TreeIter()
        newItem.user_data = value
        newItem.user_data2 = idItem
        newItem.user_data3 = 666
        return (True,item)

    def do_iter_parent(self, item):
        idSQL = self.db.sqlGetItem(
            "SELECT idParent FROM node WHERE id=? ORDER BY position LIMIT 1",
            (item.user_data2,))
        if idSQL is None:
            idSQL = 0
        item.user_data = item.user_data2
        item.user_data2 = idSQL
        return (True, item)


    def do_row_draggable(self, path):
        return False

