## FieldPicker.py - Field selection for flat text file import 
#
# (c) Remy Horton, 2021
# SPDX-License-Identifier: GPL-3.0-only


import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject

class FieldPickerHandler(object):
    def __init__(self, hdlMain):
        #self.main = hdlMain
        builder = hdlMain.builderFieldPicker
        self.dialog = builder.get_object('dialogImportText')
        builder.connect_signals(self)
        self.dialog.set_transient_for(hdlMain.window)
        self.ctrlVendor = builder.get_object('ctrlImportVendor')
        self.txtVendor = builder.get_object('txtImportVendor')
#        self.btnUp = builder.get_object('btnImportUp')
#        self.btnUp.connect('clicked', self.evtClickUp)
#        self.btnDown = builder.get_object('btnImportDown')
#        self.btnDown.connect('clicked', self.evtClickDown)
        self.modelHeaders = Gtk.ListStore(GObject.TYPE_STRING)
        self.modelHeaders.append( ["Order code"] )
        self.modelHeaders.append( ["Part number"] )
        self.modelHeaders.append( ["Manufacturer"] )
        self.modelHeaders.append( ["Description"] )
        self.listHeaders = builder.get_object('treeImportFields')
        self.listHeaders.append_column(
            Gtk.TreeViewColumn('Header',Gtk.CellRendererText(),text=0))
        self.listHeaders.set_model(self.modelHeaders)
        self.modelHeaders.connect('row-inserted',self.evtReorder1)
        self.modelHeaders.connect('row-deleted',self.evtReorder2)
        
        cell1 = Gtk.CellRendererText()
        cell2 = Gtk.CellRendererText()
        col1 = Gtk.TreeViewColumn('F',cell1,text=0)
        col2 = Gtk.TreeViewColumn('D',cell2,text=1)
        col1.set_property('min-width',5)
        self.listData = builder.get_object('treeImportData')
        self.listData.append_column(col1)
        self.listData.append_column(col2)
        self.modelData = Gtk.ListStore(
            GObject.TYPE_STRING, GObject.TYPE_STRING)
        self.listData.set_model(self.modelData)


    def evtImportBtnUp(self, button):
        itemHead,iterHead = self.listHeaders.get_selection().get_selected()
        if iterHead is None:
            return
        idxSelection = self.modelHeaders.get_path(iterHead).get_indices()[0]
        if idxSelection == 0:
            return
        path = Gtk.TreePath( [idxSelection-1] )
        iterNext = self.modelHeaders.get_iter(path)
        self.modelHeaders.swap(iterHead,iterNext)
        self.evtReorder2(None,None)

    def evtImportBtnDown(self, button):
        _,iterHead = self.listHeaders.get_selection().get_selected()
        if iterHead is None:
            return
        iterNext = self.modelHeaders.iter_next(iterHead)
        self.modelHeaders.swap(iterHead,iterNext)
        self.evtReorder2(None, None)

    def evtReorder1(self, listHead, idx, iterTree):
        path = listHead.get_path(iterTree)
        self.listHeaders.set_cursor(path,None,False)

    def evtReorder2(self, unused1, unused2):
        field1 = self.modelHeaders[0][0][0]
        field2 = self.modelHeaders[1][0][0]
        field3 = self.modelHeaders[2][0][0]
        field4 = self.modelHeaders[3][0][0]
        fields = (field1,field2,field3,field4)
        idxField = 0
        iterData = self.modelData.get_iter( [0] )
        self.modelData.set_value(iterData, 0, "?")
        while iterData is not None:
            self.modelData.set_value(iterData, 0, fields[idxField])
            idxField += 1
            if idxField > 3:
                idxField = 0
            iterData = self.modelData.iter_next( iterData )

    def process(self,listRecords,listVendors):
        self.modelData.clear()
        for r1,r2,r3,r4 in listRecords:
            self.modelData.append(['O',r1])
            self.modelData.append(['P',r2])
            self.modelData.append(['M',r3])
            self.modelData.append(['D',r4])
        self.ctrlVendor.remove_all()
        for vendor in listVendors:
            self.ctrlVendor.append_text(vendor)
        action = self.dialog.run()
        self.dialog.hide() 
        if action == Gtk.ResponseType.CANCEL:
            return None
        field1 = self.modelHeaders[0][0][0]
        field2 = self.modelHeaders[1][0][0]
        field3 = self.modelHeaders[2][0][0]
        field4 = self.modelHeaders[3][0][0]
        return (''.join([field1,field2,field3,field4]),self.txtVendor.get_text())
