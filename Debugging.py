#!/usr/bin/env python3
## Debugging.py - Debugging-related functionality
#
# (c) Remy Horton, 2021
# SPDX-License-Identifier: GPL-3.0-only

import sys
from gi.repository import Gtk

DEBUG_ENABLE = False

def printFunc(cls=None,arg=None,*more):
    if not DEBUG_ENABLE:
        return
    strFuncName = sys._getframe().f_back.f_code.co_name
    if isinstance(arg,Gtk.TreeIter):
        strFuncArgs = "Path<{0},{1},{2}>".format(
            arg.user_data, arg.user_data2, arg.user_data3)
    else:
        strFuncArgs = str(arg)
    if cls:
        print("{0}.{1}({2}):".format(type(cls).__name__,strFuncName,strFuncArgs),*more)
    else:
        print("{0}({1}): ".format(strFuncName,strFuncArgs),*more)

