## Importer.py - 
#
# (c) Remy Horton, 2021
# SPDX-License-Identifier: GPL-3.0-only


import json

def filterData(listIn,key,value):
    listOut = []
    for item in listIn:
        if item[key] == value:
            listOut.append(item)
    try:
        return sorted(listOut, key=lambda i : i['position'])
    except KeyError:
        return listOut


class Importer(object):
    def __init__(self, database):
        self.db = database

    def jsonImportV2(self,strFilename):
        try:
            fpData = open(strFilename,'rt')
            jsonData = json.loads( fpData.read() )
            fpData.close()
        except Exception as err:
            raise Exception(
                "Error loading JSON from '{0}'".format(strFilename))
        try:
            if jsonData["Program"] != "wxCatalogue" or \
                jsonData["Version"][0] != 2:
                raise Exception()
        except:
            raise Exception(
                "'{0}' is not a wxCatalogue 2.x JSON file".format(strFilename))
        self.jsonData = jsonData

        self.dictVendors = {}
        for vendor in jsonData["Vendors"]:
            self.dictVendors[ vendor['id'] ] = vendor['name']
        self.dictLocations = {}
        for location in jsonData["Locations"]:
            self.dictLocations[ location['id'] ] = location['name']
        self.dictManufacturers = {}
        for manu in jsonData["Manufacturers"]:
            self.dictManufacturers[ manu['id'] ] = manu['name']

        for category in filterData(jsonData['Categories'],'idParent',0):
            idSQL = self.db.createCategory(category['name'],None,None)
            self.jsonImportV2_ProcCat(category['id'],idSQL)

    def jsonImportV2_ProcCat(self, idParent, sqlParent):
        for subcat in filterData(self.jsonData['Categories'],'idParent',idParent):
            idSQL = self.db.createCategory(subcat['name'],sqlParent,None)
            self.jsonImportV2_ProcCat(subcat['id'],idSQL)
        listParts = filterData(self.jsonData['Components'],'idCategory',idParent)
        for part in listParts:
            idPart = part['id']
            listVend = []
            for code in filterData(self.jsonData['Ordercodes'],'idItem',idPart):
                listVend.append( (self.dictVendors[code['idVendor']],code['code']) )
            listStock = []
            for stock in filterData(self.jsonData['Stocks'],'idItem',idPart):
                listStock.append( (self.dictLocations[stock['idLocation']],stock['quantity']) )
            self.db.createPart(
                sqlParent,
                part['name'],
                self.dictManufacturers[part['idManufact']],
                part['code'],
                part['description'],
                part.get("notes",None),
                listVend,
                listStock,
                None
                )

