## EditComponent.py - Component editing dialog
#
# (c) Remy Horton, 2021
# SPDX-License-Identifier: GPL-3.0-only

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject

from NameDialog import NameDialog


class EditComponentHandler(object):
    def __init__(self, hdlMain):
        self.hdlMain = hdlMain
        self.db = hdlMain.db
        self.dialog = hdlMain.builderEditComp.get_object('dialogEditComponent')
        self.dialog.set_transient_for(hdlMain.window)
        self.builder = hdlMain.builderEditComp
        self.txtName = self.builder.get_object('txtTitle1')
        self.txtName.set_hexpand(True)
        self.txtCode = self.builder.get_object('txtCode1')
        self.txtDesc = self.builder.get_object('txtDesc1')
        self.txtNote = self.builder.get_object('txtNotes1')
        self.ctrlManu = self.builder.get_object('ctrlManu1')
        self.txtManu = self.builder.get_object('txtManu1')

        ## Vendor list ctrl
        self.modelVendors = Gtk.ListStore(str,str,int)
        self.listVendors = self.builder.get_object('treeVend')
        self.listVendors.set_model(self.modelVendors)
        # Vendor column
        cell = Gtk.CellRendererText()
        #FIXME: Decide whether to allow editing..
        #cell.set_property('editable',True)
        #cell.connect('edited',self.evtEditVendNameDone)
        col = Gtk.TreeViewColumn('Vendor',cell,text=0)
        col.set_resizable(True)
        col.set_property('min-width',100)
        self.listVendors.append_column(col)
        # Order code column
        cell = Gtk.CellRendererText()
        cell.set_property('editable',True)
        cell.connect('edited',self.evtEditVendCodeDone)
        self.listVendors.append_column(
            Gtk.TreeViewColumn('Order code',cell,text=1))

        self.modelStock = Gtk.ListStore(str,int,int)
        self.listStock = self.builder.get_object('treeStock')
        self.listStock.set_model(self.modelStock)
        # Vendor column
        cell = Gtk.CellRendererText()
        #FIXME: Allow editing??
        #cell.set_property('editable',True)
        #cell.connect('edited',self.evtEditStockLocDone)
        col = Gtk.TreeViewColumn('Stock location',cell,text=0)
        col.set_resizable(True)
        col.set_property('min-width',100)
        self.listStock.append_column(col)
        cell = Gtk.CellRendererText()
        cell.set_property('editable',True)
        cell.connect('edited',self.evtEditStockCountDone)
        self.listStock.append_column(
            Gtk.TreeViewColumn('Quantity',cell,text=1))


        self.builder.connect_signals(self)


    def evtVendAdd(self, button):
        dlgName = NameDialog(
            self.dialog,
            "Select vendor",
            "Select component vendor",
            self.listVendNames)
        strVendor = dlgName.getValue()
        if strVendor is not None:
            self.modelVendors.append([strVendor,'',-1])

    def evtVendDel(self, button):
        victim = self.listVendors.get_selection().get_selected()[1]
        if victim is not None:
            idVictim = self.modelVendors.get_value(victim,2)
            if idVictim != -1:
                self.listDeletedStock.append(idVictim)
            self.modelVendors.remove(victim)

    def evtEditVendNameDone(self, item, path, newText):
        treeiter = self.modelVendors.get_iter( path )
        self.modelVendors.set_value(treeiter, 0, newText)

    def evtEditVendCodeDone(self, item, path, newText):
        treeiter = self.modelVendors.get_iter( path )
        self.modelVendors.set_value(treeiter, 1, newText)

    def evtStockAdd(self, button):
        dlgName = NameDialog(
            self.dialog,
            "Select location",
            "Select storage location",
            self.listLocNames)
        strLoc = dlgName.getValue()
        if strLoc is not None:
            self.modelStock.append([strLoc,-1,-1])

    def evtStockDel(self, button):
        victim = self.listStock.get_selection().get_selected()[1]
        if victim is not None:
            idVictim = self.modelStock.get_value(victim,2)
            if idVictim != -1:
                self.listDeletedVend.append(idVictim)
            self.modelStock.remove(victim)

    def evtEditStockLocDone(self, item, path, newText):
        treeiter = self.modelStock.get_iter( path )
        self.modelStock.set_value(treeiter, 0, newText)

    def evtEditStockCountDone(self, item, path, newText):
        treeiter = self.modelStock.get_iter( path )
        try:
            self.modelStock.set_value(treeiter, 1, int(newText))
        except ValueError:
            pass

    def show(self, idItem):
        self.listVendNames = [v for (_,v) in self.db.getVendNames()]
        self.listLocNames = [v for (_,v,_) in self.db.getLocNames()]
        self.listDeletedVend = []
        self.listDeletedStock = []
        record = self.db.getComponent(idItem)
        if record is None:
            return False
        self.txtName.set_text(record['name'])
        self.txtCode.set_text(record['code'])
        self.txtDesc.set_text(record['desc'])
        # Some imported records have None rather than '' for empty..
        strNote = record['note']
        if strNote == None:
            strNote = ''
        self.txtNote.get_buffer().set_text(strNote)
        self.txtManu.set_text(record['manu'])
        self.ctrlManu.remove_all()
        for _,manu in self.db.getNames('manufacturer').items():
            self.ctrlManu.append_text(manu)
        self.modelVendors.clear()
        for idCode,idVend,code in record['vend']:
            strVend = self.db.getVendById(idVend)
            self.modelVendors.append( [strVend,code,idCode] )
        self.modelStock.clear()
        for idStash,idLoc,count in record['stock']:
            strLoc = self.db.getLocById(idLoc)
            self.modelStock.append( [strLoc,count,idStash] )
        action = self.dialog.run()
        self.dialog.hide()
        if action == Gtk.ResponseType.CANCEL:
            return False
        listVendors = []
        for name,code,idRec in self.modelVendors:
            listVendors.append( (name,code,idRec) )
        listStock = []
        for name,count,idRec in self.modelStock:
            listStock.append( (name,count,idRec) )
        bounds = self.txtNote.get_buffer().get_bounds()
        newrecord =  {
            'name' : self.txtName.get_text(),
            'manu' : self.txtManu.get_text(),
            'code' : self.txtCode.get_text(),
            'desc' : self.txtDesc.get_text(),
            'note' : self.txtNote.get_buffer().get_text(bounds[0],bounds[1],False),
            'vend' : listVendors,
            'stock': listStock,
            'del-vend' : self.listDeletedVend,
            'del-stock' :self.listDeletedStock
            }
        self.db.updateComponent(idItem, newrecord)
        return True
