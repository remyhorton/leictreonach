#!/usr/bin/env python3
## Leictreonach.py - Electronic component catalogue (main entrypoint)
#
# (c) Remy Horton, 2021
# SPDX-License-Identifier: GPL-3.0-only

import gi
import signal
import sqlite3
import sys
import warnings

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, GdkPixbuf, Gdk

from AddComponent import AddComponentHandler
from Database import Database
from Importer import Importer
from ManageNames import ManageNamesHandler
from FieldPicker import FieldPickerHandler
from EditComponent import EditComponentHandler
from StockModel import StockModel, StockModelList

from Debugging import printFunc


class MainWindowHandler(object):
    def __init__(self):
        self.builderMain = Gtk.Builder()
        self.builderMain.add_objects_from_file(
            "Leictreonach.glade",
            ['wndMain','imgMenuPlus1','imgMenuPlus2','imgMenuDelete']
            )
        self.builderAddComp = Gtk.Builder()
        self.builderAddComp.add_objects_from_file(
            "Leictreonach.glade",
            ['dialogAddComponent','imgIconPlus1','imgIconMinus1']
            )
        self.builderEditComp = Gtk.Builder()
        self.builderEditComp.add_objects_from_file(
            "Leictreonach.glade",
            ['dialogEditComponent',
            'imgIconPlus2','imgIconMinus2','imgIconPlus3','imgIconMinus3']
            )
        self.builderManageNames = Gtk.Builder()
        self.builderManageNames.add_objects_from_file(
            "Leictreonach.glade",
            ['dialogNames','dialogMerge']
            )
        self.builderFieldPicker = Gtk.Builder()
        self.builderFieldPicker.add_objects_from_file(
            "Leictreonach.glade",
            ['dialogImportText','listImportFields']
            )
        self.window = self.builderMain.get_object("wndMain")
        self.window.show_all()
        # Main TreeView columns
        self.treeMain = self.builderMain.get_object('treeMain')
        colTree = Gtk.TreeViewColumn("Category/component")
        self.colTree = colTree
        colIcon = Gtk.CellRendererPixbuf()
        colText = Gtk.CellRendererText()
        colText.set_property('editable',True)
        colText.connect('edited',self.evtEditName)
        self.colText = colText
        colTree.pack_start(colIcon,False)
        colTree.pack_start(colText,True)
        colTree.add_attribute(colText, "text",  0)
        colTree.add_attribute(colIcon, "pixbuf", 1)
        colTree.set_property('min-width',300)
        colTree.set_resizable(True)
        colTree.set_sizing(Gtk.TreeViewColumnSizing.FIXED)
        colTree.set_property('sizing',Gtk.TreeViewColumnSizing.FIXED)
        colTree.set_fixed_width(300)
        self.listCols = [colTree]
        self.treeMain.append_column(colTree)
        ##colTree.connect('edited',self.evtEditVendNameDone)
        def makeCol(tree, title, width, idxData):
            newCol = Gtk.TreeViewColumn(title, Gtk.CellRendererText(), text=idxData)
            newCol.set_resizable(True)
            newCol.set_property('min-width',width)
            newCol.set_property('sizing',Gtk.TreeViewColumnSizing.FIXED)
            newCol.set_fixed_width(width)
            self.listCols.append(newCol)
            tree.append_column(newCol)
        makeCol(self.treeMain,"Manufacturer",    120, 2)
        makeCol(self.treeMain,"Item code",       200, 3)
        makeCol(self.treeMain,"Order codes",     150, 4)
        makeCol(self.treeMain,"Stock locations", 100, 5)

        # Title icons
        self.imgCategory = Gtk.IconTheme.get_default().load_icon(
            "folder", Gtk.IconSize.MENU, 0)
        self.imgComponent = Gtk.IconTheme.get_default().load_icon(
            "document-new", Gtk.IconSize.MENU, 0)

        sql = sqlite3.connect("leictreonach.db")
        self.db = Database(sql)
        self.db.checkTables()

        self.menuNewCategory = self.builderMain.get_object('menuitemNewCategory')
        self.menuList = self.builderMain.get_object('menuitemListToggle')
        #self.menuLock.set_active(True)
        #self.menuLock.set_sensitive(False)

        if False:
            store = Gtk.TreeStore(str,GdkPixbuf.Pixbuf,str,str,str,str)
            root = store.append(None, ['Resis', self.imgComponent,None,None,None,None])
            store.append(root, ['Resis', self.imgComponent,None,None,None,None])
            store.append(root, ['Resis', self.imgCategory,'1','2','3','4'])
            self.treeMain.set_model(store)
        elif False:
            self.modelMain = Gtk.TreeModelSort( StockModelList(self) )
            self.treeMain.set_model(self.modelMain)
        else:
            self.modelTree = StockModel(self)
            self.modelList = StockModelList(self)

            self.modelMain = self.modelTree
            self.treeMain.set_model(self.modelMain)
            target = [('TREE_ROW', Gtk.TargetFlags.SAME_WIDGET, 0)]
            self.treeMain.enable_model_drag_source(
                    Gdk.ModifierType.BUTTON1_MASK,
                    target,
                    Gdk.DragAction.MOVE)
            self.treeMain.enable_model_drag_dest(target, Gdk.DragAction.MOVE)
            self.treeMain.connect('drag-data-get', self.modelMain.evtDragDataGet)
            self.treeMain.connect('drag-data-received', self.modelMain.evtDropDataRecv)
            self.treeMain.set_search_equal_func(self.evtSearch, None)

        self.builderMain.connect_signals(self)
        self.dlgAddComp = AddComponentHandler(self)
        self.dlgEditComp = EditComponentHandler(self)
        self.dlgEditManuNames = ManageNamesHandler(self)
        self.dlgFieldPicker = FieldPickerHandler(self)
        self.isChanges = False

    def evtSearch(self, model, idxCol, key, item, data):
        #printFunc(self, key)
        if type(model) is Gtk.TreeModelSort:
            item = model.convert_iter_to_child_iter(item)
            model = model.get_model()
        if key in model.do_get_value(item,0)[0]:
            return False
        if key in model.do_get_value(item,3):
            return False
        if key in model.do_get_value(item,4):
            return False
        return True

    def evtAddCategory(self, menu):
        itemParent = None
        position = 0
        model,item = self.treeMain.get_selection().get_selected()
        if type(model) is Gtk.TreeModelSort:
            printFunc(self,None,"Vetoed. This should not be reachable.")
            return
        if item:
            itemParent = self.modelMain.iter_parent(item).user_data
            if itemParent == 0:
                itemParent = None
            position = self.db.getItemPosition(item)
        idNew = self.db.createCategory("New category", itemParent, position)
        itemNew = self.modelMain.getItemFromId(idNew)
        path = self.modelMain.do_get_path(itemNew)
        self.modelMain.row_inserted(path, itemNew)
        self.treeMain.set_cursor(path,self.colTree,True)
        self.isChanges = True

    def evtAddComponent(self, menu):
        model,itemPos = self.treeMain.get_selection().get_selected()
        if type(model) is Gtk.TreeModelSort:
            dialogMsg = Gtk.MessageDialog(
                parent=self.window,
                flags=Gtk.DialogFlags.MODAL,
                type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.CANCEL,
                message_format="Cannot add components while in list view"
                )
            dialogMsg.set_title("Error")
            dialogMsg.connect("response", lambda w,r : dialogMsg.destroy())
            dialogMsg.show()
            return
        pathLast = None
        for idNew in self.dlgAddComp.show(itemPos):
            itemNew = model.getItemFromId(idNew)
            path = model.do_get_path(itemNew)
            model.row_inserted(path, itemNew)
            self.isChanges = True
            #path = topmodel.convert_child_path_to_path(path)
            pathLast = path
        if pathLast:
            self.treeMain.expand_to_path(pathLast)
            self.treeMain.set_cursor(pathLast)

    def evtRowClick(self, treeMain, path, col):
        model = treeMain.get_model()
        if type(model) is Gtk.TreeModelSort:
            path = model.convert_path_to_child_path(path)
            model = model.get_model()
        item = model.get_iter(path)
        if self.dlgEditComp.show(item.user_data):
            self.isChanges = True

    def evtEditName(self, cellrend, path, newText):
        model = self.treeMain.get_model()
        if type(model) is Gtk.TreeModelSort:
            path = Gtk.TreePath(path)
            path = model.convert_path_to_child_path(path)
            model = model.get_model()
        treeiter = model.get_iter(path)
        oldText = model.do_get_value(treeiter, 0)[0]
        if oldText == newText:
            return
        model.setValue(treeiter, 0, newText)
        self.isChanges = True

    def evtMenuDelete(self, menu):
        #printFunc(self)
        model,victim = self.treeMain.get_selection().get_selected()
        if victim is None:
            return
        isList = False
        if type(model) is Gtk.TreeModelSort:
            victim = model.convert_iter_to_child_iter(victim)
            model = model.get_model()
            isList = True
        path = model.do_get_path(victim)
        self.db.deleteNode(victim)
        model.row_deleted(path)
        if isList:
            #FIXME Work out how to get rid of stale item references
            self.treeMain.set_model( Gtk.TreeModelSort(model) )
        self.isChanges = True

    def evtMenuImportJSONv2(self, menu):
        dialogFile = Gtk.FileChooserDialog(
            "Load wcCatalogue JSON file",
            parent=self.window,
            action=Gtk.FileChooserAction.OPEN)
        dialogFile.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        filterFile = Gtk.FileFilter()
        filterFile.set_name("JSON files")
        #filterFile.add_mime_type('text/json')
        filterFile.add_pattern('*.json')
        dialogFile.add_filter(filterFile)
        filterFile = Gtk.FileFilter()
        filterFile.set_name("All files")
        filterFile.add_pattern('*')
        dialogFile.add_filter(filterFile)
        result = dialogFile.run()
        if result == Gtk.ResponseType.OK:
            strFilename = dialogFile.get_filename()
            dialogFile.destroy()
            imp = Importer(self.db)
            try:
                imp.jsonImportV2(strFilename)
            except Exception as err:
                raise err

                print(err)
                dialogMsg = Gtk.MessageDialog(
                    parent=self.window,
                    flags=Gtk.DialogFlags.MODAL,
                    type=Gtk.MessageType.ERROR,
                    buttons=Gtk.ButtonsType.OK,
                    message_format="error"
                    )
                dialogMsg.connect("response", lambda w,r : dialogMsg.destroy())
                dialogMsg.show()
            self.isChanges = True
            self.evtMenuRefresh(None)
        else:
            dialogFile.destroy()

    def evtMenuSave(self, menu):
        self.db.sql.commit()
        self.isChanges = False

    def evtMenuImportText(self, menu):
        dialogFile = Gtk.FileChooserDialog(
            "Load data from flat text",
            parent=self.window,
            action=Gtk.FileChooserAction.OPEN)
        dialogFile.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        filterFile = Gtk.FileFilter()
        filterFile.set_name("Text files")
        #filterFile.add_mime_type('text/json')
        filterFile.add_pattern('*.txt')
        dialogFile.add_filter(filterFile)
        filterFile = Gtk.FileFilter()
        filterFile.set_name("All files")
        filterFile.add_pattern('*')
        dialogFile.add_filter(filterFile)
        result = dialogFile.run()
        if result == Gtk.ResponseType.OK:
            strFilename = dialogFile.get_filename()
            dialogFile.destroy()
            try:
                fpData = open(strFilename,'rb')
                listLines = fpData.readlines()
                fpData.close()
                listEntries = []
                listFields = []
                for idx,line in enumerate(listLines):
                    strLine = line.decode('latin-1').strip()
                    if strLine == "":
                        if len(listFields) != 4:
                            raise Exception(
                                "Line {0}: Missing record field(s)".format(idx+1))
                        listEntries.append(
                            (listFields[0],listFields[1],listFields[2],listFields[3]))
                        listFields = []
                    elif len(listFields) > 4:
                        raise Exception(
                            "Line {0}: Excess record field(s)".format(idx+1))
                    else:
                        listFields.append(strLine)
                listVendors = [v[1] for v in sorted(self.db.getVendNames(), key=lambda x: x[0])]
                result = self.dlgFieldPicker.process(listEntries,listVendors)
                if result is None:
                    return
                # Do actual import..
                idCategory = self.db.createCategory("Imported components",None,None)
                idxDesc = result[0].index('D')
                idxPart = result[0].index('P')
                idxManu = result[0].index('M')
                idxCode = result[0].index('O')
                for entry in listEntries:
                    strDesc = entry[idxDesc]
                    strPart = entry[idxPart]
                    strManu = entry[idxManu]
                    strCode = entry[idxCode]
                    self.db.createPart(
                        idCategory,
                        strDesc,
                        strManu,
                        strPart,
                        strDesc,
                        None,
                        [ (result[1],strCode) ],
                        None,
                        None)
                itemCategory = self.modelMain.getItemFromId(idCategory)
                path = self.modelMain.do_get_path(itemCategory)
                self.modelMain.row_inserted(path, itemCategory)
                self.isChanges = True

            except Exception as err:
                print(err)
                dialogMsg = Gtk.MessageDialog(
                    parent=self.window,
                    flags=Gtk.DialogFlags.MODAL,
                    type=Gtk.MessageType.ERROR,
                    buttons=Gtk.ButtonsType.OK,
                    message_format=str(err)
                    )
                dialogMsg.connect("response", lambda w,r : dialogMsg.destroy())
                dialogMsg.show()
            self.evtMenuRefresh(None)
        else:
            dialogFile.destroy()


    # In some cases when a category is drag-drop moved it incorrectly loses
    # the expander toggle, and although it can easily be regained by closing
    # and reopening the parent category. This doesn't work for things at the
    # top-level since these items don't have a parent, so I added this force
    # reload functionality.
    def evtMenuRefresh(self, menu):
        model,item = self.treeMain.get_selection().get_selected()
        if item is not None:
            path = self.modelMain.do_get_path(item)
        else:
            path = None
        self.treeMain.set_model(None)
        self.treeMain.set_model(self.modelMain)
        if path:
            listIdx = path.get_indices()
            if len(listIdx) > 1:
                pathShort = Gtk.TreePath.new_from_indices(listIdx[:-1])
                self.treeMain.expand_to_path(pathShort)
            self.treeMain.set_cursor(path,None,False)

    def evtMenuEditManu(self, menu):
        listManu = self.db.getManuNames()
        listChanges = self.dlgEditManuNames.process(listManu,"manufacturer")
        if len(listChanges[0]) == 0 \
            and len(listChanges[1]) == 0 \
            and len(listChanges[2]) == 0:
            return
        self.db.updateManufacturers(listChanges)
        self.isChanges = True

    def evtMenuEditLocations(self, menu):
        listLocs = self.db.getLocNames()
        listChanges = self.dlgEditManuNames.process(listLocs,"location")
        if len(listChanges[0]) == 0 \
            and len(listChanges[1]) == 0 \
            and len(listChanges[2]) == 0:
            return
        self.db.updateLocations(listChanges)
        self.isChanges = True

    def evtMenuFind(self, menu):
        printFunc(self,menu,menu.get_label())
        #printFunc(self,menu,dir(menu.event))
        print(menu)
        print(menu.event)

    def evtMenuLockToggle(self, menu):
        valNew = menu.get_active()
        #printFunc(self,menu,menu.props.active)
        valOld = self.colText.get_property('editable')
        self.colText.set_property('editable',not valNew)

    def evtMenuListToggle(self, menu):
        model,item = self.treeMain.get_selection().get_selected()
        if item:
            if type(model) is Gtk.TreeModelSort:
                item = model.convert_iter_to_child_iter(item)
                model = model.get_model()
        if menu.get_active():
            self.treeMain.set_model( Gtk.TreeModelSort(self.modelList) )
            self.modelMain = self.treeMain.get_model()
            self.listCols[0].set_sort_column_id(0)
            self.listCols[1].set_sort_column_id(2)
            self.listCols[2].set_sort_column_id(3)
            self.listCols[3].set_sort_column_id(4)
            self.listCols[4].set_sort_column_id(5)
            self.menuNewCategory.set_sensitive(False)
            if item is None or model.isNodeCategory(item):
                pass
            else:
                path = self.modelList.do_get_path(
                    self.modelList.getItemFromId(item.user_data)
                    )
                path = self.modelMain.convert_child_path_to_path(path)
                #FIXME: I don't know why there is this fencepost error..
                listIdx = path.get_indices()
                path = Gtk.TreePath.new_from_indices([listIdx[0] - 1])
                self.treeMain.set_cursor(path,None,False)
        else:
            self.treeMain.set_model(self.modelTree)
            self.modelMain = self.modelTree
            for col in self.listCols:
                col.set_sort_column_id(-1)
            self.menuNewCategory.set_sensitive(True)
            if item:
                path = self.modelTree.do_get_path(
                    self.modelTree.getItemFromId(item.user_data)
                    )
                listIdx = path.get_indices()
                if len(listIdx) > 1:
                    pathShort = Gtk.TreePath.new_from_indices(listIdx[:-1])
                    self.treeMain.expand_to_path(pathShort)
                self.treeMain.set_cursor(path,None,False)


    def evtMenuAbout(self, menu):
        dialogAbout = Gtk.AboutDialog(parent=self.window)
        dialogAbout.set_name('Electronic Catalogue')
        dialogAbout.set_program_name('Leictreonach')
        dialogAbout.set_version('v3.0')
        dialogAbout.set_copyright(
            'Licenced under version 3 of the GPL\n(C) Remy Horton, 2021.')
        dialogAbout.set_comments('Electronic component stock database')
        #dialogAbout.set_license('General Public Licence v3.0')
        #dialogAbout.set_wrap_licence(True)
        dialogAbout.set_website('http://leictreonach.eu/')
        dialogAbout.set_website_label( dialogAbout.get_website() )
        #dialogAbout.set_logo()
        dialogAbout.run()
        dialogAbout.destroy()

    def evtQuit(self, *args):
        if not self.isChanges:
            return False
        dialogSave = DialogSave(self.window)
        resp = dialogSave.run()
        dialogSave.destroy()
        if resp == Gtk.ResponseType.CANCEL:
            return True
        elif resp == Gtk.ResponseType.YES:
            self.evtMenuSave(None)
        elif resp == Gtk.ResponseType.NO:
            pass
        else:
            return True
        return False

    def evtMenuQuit(self, event):
        if self.isChanges:
            dialogSave = DialogSave(self.window)
            resp = dialogSave.run()
            dialogSave.destroy()
            if resp == Gtk.ResponseType.CANCEL:
                return
            elif resp == Gtk.ResponseType.YES:
                self.evtMenuSave(None)
            elif resp == Gtk.ResponseType.NO:
                pass
            else:
                # Most likley, dialog closed via [X]. Treat as Cancel.
                return
        self.window.destroy()

    def evtDestroy(self, event):
        Gtk.main_quit()


class DialogSave(Gtk.Dialog):
    def __init__(self,parent):
        super().__init__(
            self, title="Save changes?", transient_for=parent,
            flags=Gtk.DialogFlags.MODAL)
        self.add_buttons(
            Gtk.STOCK_YES, Gtk.ResponseType.YES,
            Gtk.STOCK_NO, Gtk.ResponseType.NO,
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL
            )
        self.set_resizable(False)
        self.set_default_size(50,100)
        box = self.get_content_area()
        box.add( Gtk.Label(label="Database has changed.") )
        box.add( Gtk.Label(label="Save changes?") )
        self.show_all()


def styleLoad():
    gtkScreen = Gdk.Screen.get_default()
    gtkProvider = Gtk.CssProvider()
    gtkStyle = Gtk.StyleContext()
    gtkStyle.add_provider_for_screen(
        gtkScreen,
        gtkProvider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
    gtkProvider.load_from_data(b'''
        .expander {
            -gtk-icon-source: url("expand-plus.svg");
        }
        .expander:checked {
            -gtk-icon-source: url("expand-minus.svg");
        }
        ''')

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    warnings.filterwarnings('ignore')
    styleLoad()
    MainWindowHandler()
    Gtk.main()
