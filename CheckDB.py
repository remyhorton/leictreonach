#!/usr/bin/env python3
## CheckDB.py - Utility for checking database consistency
#
# Actual checking was moved over to Database.py but this file was kept
# as a wrapper script to avoid complications with multiple calls to
# gi.require_version() when the main program is being run.
#
# (c) Remy Horton, 2023
# SPDX-License-Identifier: GPL-3.0-only

import sqlite3
import gi
gi.require_version("Gtk", "3.0")

from Database import DatabaseCheck


if __name__ == '__main__':
    sql = sqlite3.connect("leictreonach.db")
    check = DatabaseCheck(sql)
    check.DoCheck()

