## NameDialog.py - Dialog for picking/entering name for location/vendor
#
# (c) Remy Horton, 2021
# SPDX-License-Identifier: GPL-3.0-only

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject


class NameDialog(Gtk.Dialog):
    def __init__(self, parent, title="Select name",text='Name',listNames=[]):
        super().__init__(self,parent=parent)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
        self.btnOk = self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        self.btnOk.set_sensitive(False)
        self.set_title(title)
        self.set_resizable(False)
        #self.set_default_size(900,100)
        self.get_action_area().set_property('halign',Gtk.Align.CENTER)
        box = self.get_content_area();
        box.set_orientation(Gtk.Orientation.VERTICAL)

        self.label = Gtk.Label(text)
        self.label.set_justify(Gtk.Justification.LEFT)
        self.label.set_xalign(0)
        self.label.set_margin_top(10)
        self.label.set_margin_left(10)
        self.label.set_margin_right(10)
        self.label.set_margin_bottom(10)
        self.ctrlName = Gtk.ComboBoxText.new_with_entry()
        self.ctrlName.set_margin_top(10)
        self.ctrlName.set_margin_left(20)
        self.ctrlName.set_margin_right(20)
        self.ctrlName.set_margin_bottom(30)
        self.ctrlName.connect("changed", self.evtChanged)
        #box.pack_start(self.label, False, False, 10)
        box.add(self.label)
        box.add(self.ctrlName)
        for name in listNames:
            self.ctrlName.append_text(name)
        box.show_all()


    def evtChanged(self, widget):
        # Disallow empty strings
        self.btnOk.set_sensitive( len(widget.get_active_text())>0 )


    def getValue(self):
        action = self.run()
        self.hide()
        if action == Gtk.ResponseType.OK:
            value = self.ctrlName.get_active_text()
        else:
            value = None
        self.destroy()
        return value
