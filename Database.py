## Database.py - Main database interface
#
# (c) Remy Horton, 2021
# SPDX-License-Identifier: GPL-3.0-only

import sqlite3
from Debugging import printFunc


class Database(object):
    def __init__(self, sql):
        self.sql = sql
        self.query = sql.cursor()

    def createTables(self):
        self.query.execute('''
            CREATE TABLE info (
                    id          INTEGER PRIMARY KEY,
                    name        TEXT NOT NULL UNIQUE,
                    value       TEXT
                );''')
        self.query.execute('''
            CREATE TABLE node (
                    id          INTEGER PRIMARY KEY,
                    name        TEXT NOT NULL,
                    idParent    INTEGER,
                    position    INTEGER,
                    idItem      INTEGER,
                    FOREIGN KEY(idParent) REFERENCES node(id)
                );
            ''')
        self.query.execute('''
        CREATE TABLE location (
                id          INTEGER PRIMARY KEY,
                name        TEXT NOT NULL UNIQUE
            );
            ''')
        self.query.execute('''
        CREATE TABLE vendor (
                id          INTEGER PRIMARY KEY,
                name        TEXT NOT NULL UNIQUE
            );
            ''')
        self.query.execute('''
        CREATE TABLE manufacturer (
                id          INTEGER PRIMARY KEY,
                name        TEXT NOT NULL UNIQUE
            );
            ''')
        self.query.execute('''
        CREATE TABLE component (
                id          INTEGER PRIMARY KEY,
                idManufact  INTEGER NOT NULL,
                descript    TEXT NOT NULL,
                code        TEXT NOT NULL,
                notes       TEXT,
                FOREIGN KEY(idManufact) REFERENCES manufacturer(id)
            );
            ''')
        self.query.execute('''
        CREATE TABLE ordercode (
                id          INTEGER PRIMARY KEY,
                idItem      INTEGER NOT NULL,
                idVendor    INTEGER NOT NULL,
                code        TEXT NOT NULL,
                FOREIGN KEY(idItem) REFERENCES component(id)
                FOREIGN KEY(idVendor) REFERENCES vendor(id)
            );
            ''')
        self.query.execute('''
        CREATE TABLE stock (
                id          INTEGER PRIMARY KEY,
                idItem      INTEGER NOT NULL,
                idLocation  INTEGER NOT NULL,
                count       INTEGER,
                FOREIGN KEY(idItem) REFERENCES component(id)
                FOREIGN KEY(idLocation) REFERENCES location(id)
            );
            ''')
        self.query.execute(
            "INSERT INTO info (name,value) VALUES (?,?)",
            ("Program","ElecCatalogue")
            )

    def checkTables(self):
        result = self.query.execute(
            "SELECT name FROM sqlite_master WHERE type='table' AND name='info'"
        ).fetchone()
        if result == None:
            print("Empty database. Creating..")
            try:
                self.createTables()
            except Exception as err:
                self.sql.rollback()
                raise err
            self.sql.commit()
            return
        result = self.query.execute("SELECT name,value FROM info").fetchone()
        if result is None or result[1] != "ElecCatalogue":
            raise Exception("Not an ElecCatalogue database")

    def sqlExec(self,strSQL,listParams):
        if type(listParams) is not tuple:
            listParams = (listParams,)
        self.query.execute(strSQL,listParams)
        return self.query.lastrowid

    def sqlGetRow(self,strSQL,listParams=[]):
        if listParams == []:
            return self.query.execute(strSQL).fetchone()
        if type(listParams) is not tuple:
            listParams = (listParams,)
        return self.query.execute(strSQL,listParams).fetchone()

    def sqlGetRows(self,strSQL,listParams=[]):
        if listParams == []:
            return self.query.execute(strSQL).fetchall()
        if type(listParams) is not tuple:
            listParams = (listParams,)
        return self.query.execute(strSQL,listParams).fetchall()

    def sqlGetItem(self, strSQL, listParams=[]):
        row = self.sqlGetRow(strSQL, listParams)
        if row is None:
            return None
        return row[0]


    def getItemPosition(self, item):
        if isinstance(item,int):
            return self.sqlGetRow(
                "SELECT position FROM node WHERE id IS ?",
                item)[0]
        return self.sqlGetRow(
            "SELECT position FROM node WHERE id IS ?",
            item.user_data)[0]

    def getItemParent(self, item):
        if isinstance(item,int):
            return self.sqlGetRow(
                "SELECT idParent FROM node WHERE id IS ?",
                item)[0]
        return self.sqlGetRow(
            "SELECT idParent FROM node WHERE id IS ?",
            item.user_data)[0]

    def getNodeId(self, item):
        if isinstance(item,int):
            return item
        return self.sqlGetRow(
            "SELECT id FROM node WHERE id IS ?",
            item.user_data)[0]

    def getItemIsCategory(self, item):
        idItem = self.getNodeId(item)
        if self.sqlGetRow(
            "SELECT idItem FROM node WHERE id IS ?",
            idItem)[0] is None:
            return True
        return False

    def createCategory(self, name, parent, position):
        printFunc(self,name,parent,position)
        if isinstance(parent,int) and parent == 0:
            parent = None
        if position is None:
            #FIXME: Default to top or bottom?
            position = self.sqlGetRow(
                "SELECT MAX(position)+1 FROM node WHERE idParent IS ?",
                parent)[0]
            if position is None:
                position = 1
        self.sqlExec(
            "UPDATE node SET position=position+1 WHERE idParent IS ? AND position>=?",
            (parent,position))
        self.sqlExec(
            "INSERT INTO node (name,idParent,position) VALUES (?,?,?)",
            (name,parent,position))
        return self.query.lastrowid

    def createPosition(self, parent, position=None):
        if position == -1:
            position = self.sqlGetRow(
                "SELECT MIN(position) FROM node WHERE idParent IS ?",
                parent)[0]
            if position is None:
                position = 1
        elif position is None:
            position = self.sqlGetRow(
                "SELECT MAX(position) FROM node WHERE idParent IS ?",
                parent)[0]
            if position is None:
                position = 1
            else:
                position += 1
        self.sqlExec(
            "UPDATE node SET position=position+1 WHERE idParent IS ? AND position>=?",
            (parent,position))
        return position

    def createNode(self, name, parent, position, idPart):
        position = self.createPosition(parent, position)
        return self.sqlExec(
            "INSERT INTO node (name,idParent,position,idItem) VALUES (?,?,?,?)",
            (name,parent,position,idPart))

    def _getSubcats(self, idParent):
        rows = self.sqlGetRows(
            "SELECT name,id FROM node"
            " WHERE idItem IS NULL AND idParent IS ?"
            " ORDER BY position",
            idParent)
        listSubcat = []
        for name,idSQL in rows:
            subs = self._getSubcats(idSQL)
            listSubcat.append( (name,idSQL,subs) )
        return listSubcat

    def getCategories(self):
        return self._getSubcats(None)

    def getOrCreateNameId(self, strTable, strName):
        row = self.query.execute(
            "SELECT id FROM {0} WHERE name IS ?".format(strTable),
            (strName,)).fetchone()
        if row is not None:
            return row[0]
        self.query.execute(
            "INSERT INTO {0} (name) VALUES (?)".format(strTable),
            (strName,))
        return self.query.lastrowid

    def createPart(self, idParent, name, manu, code, desc, note, vend, stock, pos):
        if False:
            print("New part (idParent:{0})".format(idParent))
            print("  Name:",name)
            print("  Manu:",manu)
            print("  Code:",code)
            print("  Desc:",desc)
            print("  Note:",note)
            print("  Vend:",vend)
            print("  Stock:",stock)
        idManu = self.getOrCreateNameId('manufacturer', manu)
        idPart = self.sqlExec(
            "INSERT INTO component" +
            " (idManufact,descript,code,notes)" +
            " VALUES (?,?,?,?)",
            (idManu,desc,code,note))
        idNew = self.createNode(name, idParent, pos, idPart)
        if vend is not None:
            for nameVend,codeVend in vend:
                idVend = self.getOrCreateNameId('vendor',nameVend)
                self.sqlExec(
                    "INSERT INTO ordercode (idItem,idVendor,code) VALUES (?,?,?)",
                    (idPart,idVend,codeVend))
        if stock is not None:
            for nameLoc,count in stock:
                idLoc = self.getOrCreateNameId('location',nameLoc)
                self.sqlExec(
                    "INSERT INTO stock (idItem,idLocation,count) VALUES (?,?,?)",
                    (idPart,idLoc,count))
        return idNew

    def deleteNode(self, itemVictim):
        if not isinstance(itemVictim,int):
            idVictim = itemVictim.user_data
        else:
            idVictim = itemVictim
        listChildren = self.sqlGetRows(
            "SELECT id FROM node WHERE idParent IS ?",
            idVictim)
        for idChild in listChildren:
            self.deleteNode(idChild[0])
        position = self.getItemPosition(itemVictim)
        idParent = self.getItemParent(itemVictim)
        self.sqlExec(
            "UPDATE node SET position=position-1 WHERE idParent IS ? AND position>?",
            (idParent,position))
        idPart = self.sqlGetRow(
            "SELECT idItem FROM node WHERE id=?",
            (idVictim,))[0]
        if idPart is not None:
            self.sqlExec("DELETE FROM component WHERE id=?",(idPart,))
            self.sqlExec("DELETE FROM ordercode WHERE idItem=?",(idPart,))
            self.sqlExec("DELETE FROM stock     WHERE idItem=?",(idPart,))
        self.sqlExec("DELETE FROM node WHERE id=?",(idVictim,))

    def getNames(self, strType):
        listNames = dict()
        for idSQL,name in self.sqlGetRows(
            "SELECT id,name FROM {0} ORDER BY name".format(strType)):
            listNames[idSQL] = name
        return listNames

    def getManuNames(self):
        listManu = []
        for idManu,nameManu in self.sqlGetRows(
            "SELECT id,name FROM manufacturer ORDER BY name"):
            if self.sqlGetRow(
                "SELECT COUNT(*) FROM component WHERE idManufact=?",
                (idManu,))[0] > 0:
                inUse = True
            else:
                inUse = False
            listManu.append( (idManu,nameManu,inUse) )
        return listManu

    def getVendNames(self):
        listRows = self.sqlGetRows(
            "SELECT id,name FROM vendor ORDER BY name")
        return [n for n in listRows]

    def getLocNames(self):
        listLoc = []
        for idLoc,nameLoc in self.sqlGetRows(
            "SELECT id,name FROM location ORDER BY name"):
            if self.sqlGetRow(
                "SELECT COUNT(*) FROM stock WHERE idLocation=?",
                (idLoc,))[0] > 0:
                inUse = True
            else:
                inUse = False
            listLoc.append( (idLoc,nameLoc,inUse) )
        return listLoc


    def getManuById(self, idManu):
        return self.sqlGetRow(
            "SELECT name FROM manufacturer WHERE id=?",
            (idManu))[0]

    def getVendById(self, idVend):
        return self.sqlGetRow(
            "SELECT name FROM vendor WHERE id=?",
            (idVend))[0]

    def getLocById(self, idLoc):
        return self.sqlGetRow(
            "SELECT name FROM location WHERE id=?",
            (idLoc))[0]


    def getNodeName(self, idNode):
        return self.sqlGetRow(
            "SELECT name FROM node WHERE id=?",
            (idNode))[0]

    def setNodeName(self, idNode, newName):
        self.sqlExec(
            "UPDATE node SET name=? WHERE id=?",
            (newName,idNode))


    def getComponent(self, idItem):
        strName,idPart = self.sqlGetRow(
            "SELECT name,idItem FROM node WHERE id=?",
            (idItem,))
        if idPart is None:
            return None
        rowPart = self.sqlGetRow(
            "SELECT idManufact,descript,code,notes FROM component WHERE id=?",
            (idPart,))
        listVend = self.sqlGetRows(
            "SELECT id,idVendor,code FROM ordercode WHERE idItem=?",
            (idPart,))
        listStock = self.sqlGetRows(
            "SELECT id,idLocation,count FROM stock WHERE idItem=?",
            (idPart,))
        return {
            'name' : strName,
            'manu' : self.getManuById(rowPart[0]),
            'code' : rowPart[2],
            'desc' : rowPart[1],
            'note' : rowPart[3],
            'vend' : listVend,
            'stock': listStock
            }

    def updateComponent(self, idItem, dictValues):
        idPart = self.sqlGetRow(
            "SELECT idItem FROM node WHERE id=?",
            (idItem,))[0]
        self.setNodeName(idItem,dictValues['name'])
        idManu = self.getOrCreateNameId('manufacturer',dictValues['manu'])
        self.sqlExec(
            "UPDATE component SET idManufact=?,descript=?,code=?,notes=? WHERE id=?",
            (idManu,dictValues['desc'],dictValues['code'],dictValues['note'],idPart))
        for victim in dictValues['del-stock']:
            self.sqlExec("DELETE FROM ordercode WHERE id=?",(victim,))
        for victim in dictValues['del-vend']:
            self.sqlExec("DELETE FROM stock WHERE id=?",(victim,))

        for name,code,idRec in dictValues['vend']:
            idVend = self.getOrCreateNameId('vendor',name)
            if idRec == -1:
                self.sqlExec(
                    "INSERT INTO ordercode (idVendor,code,idItem) VALUES (?,?,?)",
                    (idVend,code,idPart))
                pass
            else:
                self.sqlExec(
                    "UPDATE ordercode SET idVendor=?,code=? WHERE id=?",
                    (idVend,code,idRec))

        for name,count,idRec in dictValues['stock']:
            idLoc = self.getOrCreateNameId('location',name)
            if idRec == -1:
                self.sqlExec(
                    "INSERT INTO stock (idLocation,count,idItem) VALUES (?,?,?)",
                    (idLoc,count,idPart))
                pass
            else:
                self.sqlExec(
                    "UPDATE stock SET idLocation=?,count=? WHERE id=?",
                    (idLoc,count,idRec))

    def updateManufacturers(self, listChanges):
        for idName,newName in listChanges[0]:
            self.sqlExec(
                "UPDATE manufacturer SET name=? WHERE id=?",
                (newName,idName))
        for idDest,listVictims in listChanges[1]:
            for idVictim in listVictims:
                self.sqlExec(
                    "UPDATE component SET idManufact=? WHERE idManufact=?",
                    (idDest,idVictim))
                self.sqlExec(
                    "DELETE FROM manufacturer WHERE id=?",
                    (idVictim,))
        for idVictim in listChanges[2]:
            self.sqlExec(
                "DELETE FROM manufacturer WHERE id=?",
                (idVictim,))

    def updateLocations(self, listChanges):
        for idName,newName in listChanges[0]:
            self.sqlExec(
                "UPDATE location SET name=? WHERE id=?",
                (newName,idName))
        for idDest,listVictims in listChanges[1]:
            for idVictim in listVictims:
                self.sqlExec(
                    "UPDATE stock SET idLocation=? WHERE idLocation=?",
                    (idDest,idVictim))
                self.sqlExec(
                    "DELETE FROM location WHERE id=?",
                    (idVictim,))
        for idVictim in listChanges[2]:
            self.sqlExec(
                "DELETE FROM location WHERE id=?",
                (idVictim,))


class DatabaseCheck(object):
    def __init__(self, sql):
        self.query = sql.cursor()
        self.setSeen = set()

    def sqlGetRow(self,strSQL,listParams=[]):
        if listParams == []:
            return self.query.execute(strSQL).fetchone()
        if type(listParams) is not tuple:
            listParams = (listParams,)
        return self.query.execute(strSQL,listParams).fetchone()

    def sqlGetRows(self,strSQL,listParams=[]):
        if listParams == []:
            return self.query.execute(strSQL).fetchall()
        if type(listParams) is not tuple:
            listParams = (listParams,)
        return self.query.execute(strSQL,listParams).fetchall()

    def DoCheck(self):
        self.checkChilds(None)
        for idNode in self.sqlGetRows("SELECT id,name,idParent FROM node"):
            if idNode[0] not in self.setSeen:
                print("Orphan node",idNode)
        self.checkNodeItem()
        self.checkComponentNotParent()
        self.checkOrphanComponents()
        self.checkUnusedNames()
        self.checkDuplicateParts()

    def checkChilds(self, idParent):
        listRoot = self.sqlGetRows(
            "SELECT * FROM node WHERE idParent IS ? ORDER BY position",
            idParent
            )
        self.checkList(idParent, listRoot)
        for idNode in listRoot:
            self.checkChilds(idNode[0])

    def checkList(self, idParent, listNodes):
        listIDs = [node[0] for node in listNodes]
        listPos = [node[3] for node in listNodes]
        if listPos == []:
            return

        # Leictreonach itself uses zero-based indexing for positioning but its
        # predecessor wxCatalogue used one-based indexing, so allow for both..
        listStart = listPos[0]
        listEnd = len(listPos)
        if listPos[0] == 1:
            listEnd += 1
        listExpected = list(range(listStart,listEnd))
        if listExpected != listPos:
            # Ideally all the children under a node should have contigious
            # position indices but all that really matters is there are no
                # duplicates.goo
            print("Non-contigious positions in node {0}".format(idParent))
            print("  Database:",listPos)
            print("  Extected:",listExpected)
        for idNode in listIDs:
            self.setSeen.add(idNode)

    def checkNodeItem(self):
        # Not sure why idItem was not declared as foreign key in the first
        # place but that is how it is with existing databases.
        listBad = self.sqlGetRows(
            'SELECT id FROM node WHERE idItem IS NOT NULL' +
            ' AND NOT EXISTS(SELECT 1 FROM component WHERE id == idItem)'
            )
        for bad in listBad:
            print('Node {0} references non-existent component'.format(listBad[0]))

    def checkComponentNotParent(self):
        # Nodes that represent components cannot be parents of other nodes
        listBad = self.sqlGetRows(
            'SELECT id as id2 FROM node WHERE idItem IS NOT NULL' +
            ' AND EXISTS(SELECT 1 FROM node WHERE idParent = id2)'
            )
        for bad in listBad:
            print('Node {0} is both category and component'.format(listBad[0]))

    def checkOrphanComponents(self):
        # Checks for both orphaned components and components that are
        # referenced  by more than one node.
        listBad = self.sqlGetRows(
            'SELECT id as idComponent FROM component'+
            ' WHERE (SELECT count(id) FROM node WHERE idItem == idComponent) != 1;'
            )
        for bad in listBad:
            print('Component {0} not referenced by exactly one node'.format(
                    listBad[0]
                    )
                )

    def checkUnusedNames(self):
        # Unused vendor/manufacturer/location names are not errors since
        # they can be created through normal operation.
        listUnused = self.sqlGetRows(
            'SELECT id as idLoc,name FROM location' +
            ' WHERE NOT EXISTS(SELECT 1 FROM stock WHERE idLocation == idLoc);'
            )
        for unused in listUnused:
            print('Location {0} ({1}) is unused'.format(unused[0],unused[1]))

        listUnused = self.sqlGetRows(
            'SELECT id idVen,name FROM vendor' +
            ' WHERE NOT EXISTS(SELECT 1 FROM ordercode WHERE idVendor == idVen);'
            )
        for unused in listUnused:
            print('Vendor {0} ({1}) is unused'.format(unused[0],unused[1]))

        listUnused = self.sqlGetRows(
            'SELECT id as idMan,name FROM manufacturer' +
            ' WHERE NOT EXISTS(SELECT 1 FROM component WHERE idManufact == idMan);'
            )
        for unused in listUnused:
            print('Manufacturer {0} ({1}) is unused'.format(unused[0],unused[1]))

    def checkDuplicateParts(self):
        # Unused vendor/manufacturer/location names are not errors since
        # they can be created through normal operation.
        listDupCodes = self.sqlGetRows(
            'SELECT code,COUNT(*) AS cnt FROM component' +
            ' GROUP BY code HAVING cnt > 1;'
            )
        for code in listDupCodes:
            print('Duplicate component "{0}"'.format(code[0]))
            listItems = self.sqlGetRows(
                'SELECT node.name,parent.id FROM node' +
                ' JOIN (SELECT id AS idCmp FROM component AS dup WHERE code = ?)' +
                ' ON idCmp = node.idItem' +
                ' JOIN node AS parent ON parent.id = node.idParent;',
                code[0]
                )
            for node in listItems:
                print('   {0}: {1}'.format(
                    ' -> '.join(self.getPath(node[1])),node[0]
                    )
                )

    def getPath(self, idParent):
        if idParent is None:
            return []
        row = self.sqlGetRow(
            'SELECT name,idParent FROM node WHERE id = ?;',
            idParent
            )
        listParents = self.getPath(row[1]);
        listParents.append( row[0] )
        return listParents
